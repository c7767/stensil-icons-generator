# Stencil Icon Components

A design system can also incorporate a full set of Icons. Preferably those icons can also be web-components just like the rest of the design system set up using Stencil.

To automatically create the source code for icon web components for a full folder of SVG files, this plugin comes in handy. Just add it to your stencil project and generate the icons.

### Install

```bash
yarn add git+https://github.com/group/express.git
```

Add a command (script) to your package.json to create the components like;

```json
scripts: {
    ...
    "stencil:icons": "stencil-icon-components --src assets/icons --dest src/components/icons",
    ...
}
```

### Options

| option         | description                                                                                                                                                                                                | default                 |
| -------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------- |
| `--src`        | Source folder with SVG files                                                                                                                                                                               | `/src/assets/icons`     |
| `--dest`       | Destination folder for components                                                                                                                                                                          | `/src/components/icons` |
| `--prefix`     | Add a prefix to all files, ex; social-network.svg becomes icon-social-network                                                                                                                              | `false`                 |
| `--remove-old` | Remove the whole destionation folder as set. In order to be sure to not have any old files and create everything new. Don't set this if your destination folder also includes files which arent generated. | `false`                 |

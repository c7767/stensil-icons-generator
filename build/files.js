const svgtojsx = require("svg-to-jsx");
const { kebabCase, PascalCase, fileName } = require("./helpers.js");
const { CleanUpSVG } = require("./helpers");

const BUILD = {};

BUILD.SPEC = async (fileData, options) => {
  const component = PascalCase(
    (options.prefix ? options.prefix : "icon-") + fileName(fileData.name)
  );
  const file = `import { ${component} } from './${kebabCase(
    fileName(fileData.name)
  )}';

    describe('${options.prefix ? options.prefix : "icon-"}${kebabCase(
    fileName(fileData.name)
  )}', () => {
        it('builds', () => {
        expect(new ${component}()).toBeTruthy();
        });
    });
    `;
  return file;
};

BUILD.E2E = async (fileData, options) => {
  const file = `import { newE2EPage } from '@stencil/core/testing';

    describe('icon-${kebabCase(fileName(fileData.name))}', () => {
        it('renders', async () => {
        const page = await newE2EPage();
        await page.setContent('<${
          options.prefix ? options.prefix : "icon-"
        }${kebabCase(fileName(fileData.name))}></${
    options.prefix ? options.prefix : "icon-"
  }${kebabCase(fileName(fileData.name))}>');
        await page.waitForChanges();
    
        const element = await page.find('${
          options.prefix ? options.prefix : "icon-"
        }${kebabCase(fileName(fileData.name))}');
        expect(element).toHaveClass('hydrated');
        });
    });
`;
  return file;
};

BUILD.CSS = async (fileData, options) => {
  const file = `
  svg {
    fill: var(--icon-color, #343841);
    display: block;
    margin: auto;
    width: var(--icon-width,16px);
    height: var(--icon-height,16px);
  }
`;
  return file;
};

BUILD.TSX = async (fileData, options) => {
  return await svgtojsx(fileData.data).then(function (jsx) {
    jsx = CleanUpSVG(jsx);
    const currentFileName = `${
      options.prefix ? options.prefix : "icon-"
    }${kebabCase(fileName(fileData.name))}`;
    const file = `import { Component, Element, h, Host, Prop } from '@stencil/core';
    
@Component({
  tag: '${currentFileName}',
  styleUrl: '${kebabCase(fileName(fileData.name))}.css',
  shadow: true
})


export class ${PascalCase(currentFileName)} {

    @Prop() width: any;
    @Prop() height: any;
    @Prop() color: string;
    @Prop() size: "xs"|"sm"|"md" = "sm";
  
    @Element() IconElement: SVGSVGElement;

    private SvgIcon: SVGSVGElement;

    applyProps(){
      this.SvgIcon = this.IconElement.shadowRoot.querySelector('svg');
      if (!this.SvgIcon) {
        return;
      }
      // Set Style Props
      let [_width, _height] = [this.width, this.height];
      const _color = getComputedStyle(document.documentElement).getPropertyValue(\`--cf-color-\${
        this.color
      }\`);
      if (this.size) {
        _width = _width || getComputedStyle(document.documentElement).getPropertyValue(\`--cf-icon-width-\${
          this.size
        }\`);
        _height = _height || getComputedStyle(document.documentElement).getPropertyValue(\`--cf-icon-height-\${
          this.size
        }\`);
      }
  
      if (_width) this.SvgIcon.style.setProperty('--icon-width', _width);
      if (_height) this.SvgIcon.style.setProperty('--icon-height', _height);
      if (_color) this.SvgIcon.style.setProperty('--icon-color', _color);    
    }
    
    componentDidLoad(){
    this.applyProps();
    }
    
    componentDidUpdate(){
    this.applyProps();
    }
    
      
    render() {

    return (
        <Host>
            ${jsx}
        </Host>);
  }
}
`;
    return file;
  });
};

BUILD.STORYBOOK = async (fileData, options) => {
  const currentFileName = `${
    options.prefix ? options.prefix : "icon-"
  }${kebabCase(fileName(fileData.name))}`;

  const file = `
    import { html } from 'lit-html';

    export default {
      title: 'Icons/${kebabCase(fileName(fileData.name))}',
      component: '${currentFileName}',
      argTypes: {
        size: {
          options: ['xs', 'sm', 'md'],
          control: { type: 'radio' }
        },
        color: {
        options: [
          'content-default',
          'content-muted',
          'content-success',
          'content-warning',
          'content-disabled',
          'content-inverted-default',
          'content-inverted-muted',
          'content-inverted-success',
          'content-inverted-warning',
          'content-inverted-disabled',
        ],
          control: { type: 'select' }
        }
      }
    };

    export const Default = args => html\`<${currentFileName} color="\${args.color}" size="\${args.size}"></${currentFileName}>\`;

    Default.args = {
      color: 'content-default',
      size: 'sm'
    };
    
    export const WithWidth = args => html\`<${currentFileName} color="\${args.color}" width="\${args.width}" height="\${args.height}"></${currentFileName}>\`
    
    WithWidth.args = {
      color: 'content-default',
      height: '24px',
      width: '24px'
    };
    `;
  return file;
};

BUILD.DOCS = async (files, options) => {
  const elements = files
    .map((x) => `<cf-icon-${kebabCase(fileName(x.name))} />`)
    .join(",");
  const file = `
import { Meta, Preview } from '@storybook/addon-docs/blocks';

<Meta title="Icons docs" />

<div>
  <Preview>
  {[${elements}].map(el => (
    <div key={el.key}>
      {el}
    </div>
  ))}
  </Preview>
</div>
    `;
  return file;
};

module.exports = BUILD;
